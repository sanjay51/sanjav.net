<html>
	<head>
		<title>URL Shortner with hit counter - sanjav.net</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		<script src='https://www.google.com/recaptcha/api.js'></script>
	</head>
	<body>
		<div class="container" align="center">
			<h2>URL Shortner - with hit counter</h2>

			<div class="container2" align="left">
				<form role="form" action="shortner-process.php" method="get">
					<label for="longurl">URL: </label>
					<input id="longurl" name="longurl" class="form-control" type="text">
					</input>
					<br/>
					<label for="shorturl">Shorten to:</label>
					sanjav.net/
					<input id="shorturl" class="form-control" type="text" name="shorturl">
					</input>
					<br/>
					<div class="g-recaptcha" data-sitekey="6Lfqyg0TAAAAAJjO-0APoWpxywdfN9khKv3SNM7A"></div><br/>
					<input class="btn btn-default" type="submit" value="Shorten">
					</input>
				</form>
			</div>
		</div>
	</body>
</html>