//keeping this method to be retirevable for DOM manipulation (onclick)
executeReplace = 10;
//any random number, important thing is that it's global variable

padModule.controller('SearchPanelController', function($scope, Constants, BroadcastService) {
	$scope.searchPanelObject = {};
	$scope.searchPanelObject.isSearchPanelVisible = false;

	$scope.searchPanelObject.originalData = "";
	$scope.searchPanelObject.modifiedData = "";

	$scope.$on(Constants.search_in_file_operation, function() {
		$scope.searchPanelObject.showSearchPanel(true);

		$scope.searchPanelObject.originalData = $scope.getCurrentFileData();
		$scope.searchPanelObject.modifiedData = $scope.searchPanelObject.originalData;

		//update view
		$scope.searchPanelObject.data = $scope.searchPanelObject.originalData;
	});

	$scope.searchPanelObject.closeOperation = function() {
		$scope.searchPanelObject.showSearchPanel(false);
	};

	$scope.searchPanelObject.applyChangesOperation = function() {
		$scope.setCurrentFileData($scope.searchPanelObject.modifiedData);
		$scope.searchPanelObject.showSearchPanel(false);
	};

	$scope.searchPanelObject.resetOperation = function() {
		$scope.searchPanelObject.data = $scope.searchPanelObject.originalData;
		$scope.searchPanelObject.modifiedData = $scope.searchPanelObject.originalData;

		//We need to do this via javascript, until we find a solution of <br> through angular
		divSearchPanelDataJSHandle = document.getElementById("divSearchPanelData");
		divSearchPanelDataJSHandle.innerHTML = $scope.searchPanelObject.data;
	};

	$scope.searchPanelObject.replaceAllOperation = function() {
		searchString = $scope.searchPanelObject.searchString;
		replaceString = $scope.searchPanelObject.replaceString;
		currentData = $scope.searchPanelObject.modifiedData;

		//We need to do this via javascript, until we find a solution of <br> through angular
		divSearchPanelDataJSHandle = document.getElementById("divSearchPanelData");

		newData = currentData;
		if (!(searchString == undefined || searchString == "")) {

			token = "____";

			//replace searchString with token first, to avoid loop.
			while (newData.indexOf(searchString) != -1) {
				newData = newData.replace(searchString, token);
			}

			while (newData.indexOf(token) != -1) {
				newData = newData.replace(token, replaceString);
			}
		}

		//update view
		$scope.searchPanelObject.data = newData;
		divSearchPanelDataJSHandle.innerHTML = newData;

		$scope.searchPanelObject.modifiedData = newData;
	};

	$scope.searchPanelObject.searchText = function() {
		searchString = $scope.searchPanelObject.searchString;
		replaceString = $scope.searchPanelObject.replaceString;
		currentData = $scope.searchPanelObject.modifiedData;

		//We need to do this via javascript, until we find a solution of <br> through angular
		divSearchPanelDataJSHandle = document.getElementById("divSearchPanelData");

		newData = currentData;
		if (!(searchString == undefined || searchString == "")) {

			token = "____";

			//replace searchString with token first, to avoid loop.
			while (newData.indexOf(searchString) != -1) {
				newData = newData.replace(searchString, token);
			}

			//replace token with decorated searchString
			count = 0;
			while (newData.indexOf(token) != -1) {

				if (replaceString == undefined || replaceString == "") {
					replaceText = "<b style='background-color:blue'>" + searchString + "</b>";
				} else {
					replaceText = "<small><strike style='background-color:red'>" + searchString + "</strike></small><b style='background-color:blue'>" + replaceString + "</b>" + "<button onclick='executeReplace(" + count + ")' class='btn btn-warning btn-xs'>Replace</button>";
				}

				count++;
				newData = newData.replace(token, replaceText);
			}
		}

		//update view
		$scope.searchPanelObject.data = newData;
		divSearchPanelDataJSHandle.innerHTML = newData;
	};

	$scope.searchPanelObject.executeReplace = function(index) {
		searchString = $scope.searchPanelObject.searchString;
		replaceString = $scope.searchPanelObject.replaceString;
		currentData = $scope.searchPanelObject.modifiedData;

		//We need to do this via javascript, until we find a solution of <br> through angular
		divSearchPanelDataJSHandle = document.getElementById("divSearchPanelData");

		newData = currentData;
		if (!(searchString == undefined || searchString == "")) {

			token = "____";

			//replace searchString with token first, to avoid loop.
			count = 0;
			while (newData.indexOf(searchString) != -1 && count <= index) {
				if (count == index) {
					newData = newData.replace(searchString, replaceString);
				} else {
					newData = newData.replace(searchString, token);
				}

				count++;
			}

			//replace token with decorated searchString
			while (newData.indexOf(token) != -1) {
				newData = newData.replace(token, searchString);
			}
		}

		//update view
		$scope.searchPanelObject.data = newData;
		divSearchPanelDataJSHandle.innerHTML = newData;
		$scope.searchPanelObject.modifiedData = newData;

		$scope.searchPanelObject.searchText();
	};

	$scope.searchPanelObject.showSearchPanel = function(bool) {
		$scope.searchPanelObject.isSearchPanelVisible = bool;
	};

	executeReplace = $scope.searchPanelObject.executeReplace;
});

padModule.directive('searchPanel', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'panel/_searchpanel.html',
		controller : 'SearchPanelController'
	};
});
