headerModule.controller('HeaderController', function ($scope, UserDataService, BroadcastService, TimeLoopService, Constants) {
	$scope.successButtonClass = "btn btn-success";
	$scope.dangerButtonClass = "btn btn-danger";
	$scope.saveButtonClass = $scope.dangerButtonClass;
	
	$scope.isAutoSyncEnabled = true;
	
	$scope.newWorkspaceOperation = function() {
		BroadcastService.broadcast(Constants.new_workspace_operation);
	};
	
	$scope.saveWorkspace = function() {
		BroadcastService.broadcast(Constants.save_operation);
	};
	
	$scope.openWorkspaceOperation = function() {
		BroadcastService.broadcast(Constants.open_workspace_operation);
	};
	
	$scope.undoOperation = function() {
		BroadcastService.broadcast(Constants.undo_operation);
	};
	
	$scope.redoOperation = function() {
		BroadcastService.broadcast(Constants.redo_operation);
	};
	
	$scope.cutOperation = function() {
		BroadcastService.broadcast(Constants.cut_operation);
	};
	
	$scope.openOperation = function() {
		BroadcastService.broadcast(Constants.open_operation);
	};
	
	$scope.renameFileOperation = function() {
		BroadcastService.broadcast(Constants.rename_file_operation);
	};
	
	$scope.showLocalHistory = function() {
		BroadcastService.broadcast(Constants.show_local_history);
	};
	
	$scope.searchInFileOperation = function() {
		BroadcastService.broadcast(Constants.search_in_file_operation);
	};
	
	$scope.toggleAutoSync = function() {
		if ($scope.isAutoSyncEnabled == true) {
			TimeLoopService.stopAutoSyncWorkspaceWithServer();
			$scope.isAutoSyncEnabled = false;
		} else {
			TimeLoopService.startAutoSyncWorkspaceWithServer();
			$scope.isAutoSyncEnabled = true;
		}
	};

	$scope.$on(Constants.save_success, function() {
		$scope.saveButtonClass = $scope.successButtonClass;
	});

	$scope.$on(Constants.unsaved_changes, function() {
		$scope.saveButtonClass = $scope.dangerButtonClass;
	});
});

headerModule.directive('headerBox', function() {
    return {
        restrict: 'E',
        scope: {
            info: '='
        },
        controller: "HeaderController",
        templateUrl:'header/_header.html'
    };
});