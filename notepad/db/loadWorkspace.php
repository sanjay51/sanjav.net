<?php
include 'queries/getWorkspaceByIdQuery.php';
include (__DIR__).'/obfuscation/obfuscate.php';

if (isDevo()) {
	ini_set('display_startup_errors', 1);
	ini_set('display_errors', 1);
	error_reporting(-1);
}

$id = getDecryptedID($_GET['id']);

//TODO: Validate input here
echo getWorkspaceByIdQuery($id);
?>
