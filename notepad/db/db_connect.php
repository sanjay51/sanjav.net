<?php

function isDevo() {
	return startsWith($_SERVER["REMOTE_ADDR"], "127");
}

function getDBConnection() {
	static $db = null;

	if ($db != null) {
		return $db;
	}
	
	if (isDevo()) {
		//Try local db settings
		$servername = "localhost";
		$username = "root";
		$port = 8889;
		$password = "root";
		$dbname = "1066258_pico";
	} else {
		$servername = "fdb2.awardspace.com";
		$username = "1066258_pico";
		$port = 3306;
		$password = "Haryana1*";
		$dbname = $username;
	}

	// Create connection
	$db = new mysqli($servername, $username, $password, $dbname, $port);

	// Check connection
	if ($db -> connect_error) {
		die("Connection error with database. Please try again :(");
	}

	return $db;

}

function startsWith($haystack, $needle) {
    // search backwards starting from haystack length characters from the end
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
}