<?php
include 'queries/saveWorkspaceQuery.php';

if (isDevo()) {
	ini_set('display_startup_errors', 1);
	ini_set('display_errors', 1);
	error_reporting(-1);
}

$data = json_decode(file_get_contents("php://input"));

//TODO: Validate input here
echo saveWorkspace($data);
?>
