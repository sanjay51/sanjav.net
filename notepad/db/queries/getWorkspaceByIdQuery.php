<?php
include (__DIR__).'/../db_connect.php';

function getWorkspaceByIdQuery($workspaceId) {
	$queryWorkspace = "SELECT email FROM  `sanjavnet_notepad_workspaces` where id='$workspaceId'";
	$queryFiles = "SELECT file_index, file_name, data FROM  `sanjavnet_notepad_files` where workspace_id='$workspaceId'";

	$db = getDBConnection();
	$result = $db -> query($queryWorkspace);
	
	if ($result->num_rows <= 0) {
		return null;
	}

	$row = mysqli_fetch_assoc($result);
	$email = json_encode($row["email"]);
	$out = "{" . "\"email\" : $email, " . "\"files\" : [";

	$result = $db -> query($queryFiles);
	
	if ($result -> num_rows > 0) {
		// output data of each row
		$count = 0;
		while ($row = $result -> fetch_assoc()) {
			$file_index = json_encode($row['file_index']);
			$file_name = json_encode($row['file_name']);
			$data = json_encode($row['data']);
			
			//append a coma
			if ($count != 0) {
				 $out .= ","; 
			}
			
			$out .= "{ \"file_index\" : $file_index, \"file_name\" : $file_name, \"data\" : $data }";
			$count++;
		}
	}
	$out .= "] }";
	return $out;
}
?>