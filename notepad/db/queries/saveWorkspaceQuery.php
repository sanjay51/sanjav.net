<?php
include (__DIR__).'/../db_connect.php';

//overwrite existing workspace
function saveWorkspace($workspace) {
	$db = getDBConnection();
	if (! isset($workspace -> workspace_id)) {
		return createWorkspace($workspace);
	} else {
		$workspace_id = $workspace -> workspace_id;
		//update existing workspace
		$query1 = "UPDATE sanjavnet_notepad_workspaces set last_modified_date = CURRENT_TIMESTAMP WHERE id= $workspace_id;";
		$result = $db -> query($query1);

		//get file count in workspace
		$query2 = "select count(*) as total from sanjavnet_notepad_files WHERE workspace_id=$workspace_id;";
		$result = $db -> query($query2);
		$totalFileCount = mysqli_fetch_assoc($result)['total'];

		$index = 1;
		foreach ($workspace->files as $file) {
			$file_index = $index - 1;
			$file_name = $db -> real_escape_string($file -> file_name);
			$data = $db -> real_escape_string($file -> data);
			$query = "";
			if ($index <= $totalFileCount) {
				//Update file then
				$query = "Update sanjavnet_notepad_files set file_index=$file_index, file_name = '$file_name', data='$data' where workspace_id=$workspace_id and file_index = $file_index;";
			} else {
				$query = "insert into sanjavnet_notepad_files (workspace_id, file_index, file_name, data, creation_date)
				          VALUES ($workspace_id, $file_index, '$file_name', '$data', CURRENT_TIMESTAMP)";
			}
			
			$db->query($query);

			$index++;
			$query1 .= $query;
		}

		return $query1 . "\n" . $query2;
	}
}

//create new workspace
function createWorkspace($workspace) {
	
	$db = getDBConnection();
	
	if ($workspace -> email != null) {
		//create workspace
		$email = $workspace -> email;
		$query = "insert into sanjavnet_notepad_workspaces (email) VALUES ('$email')";
		$result = $db -> query($query);
		
		//select and set workspace_id
		$query = "select id from sanjavnet_notepad_workspaces where email = '$email'";
		$result = $db -> query($query);
		$workspace_id = mysqli_fetch_assoc($result)['id'];
		
		//attach workspace id with workspace object
		$workspace -> workspace_id = $workspace_id;
		
		//saveWorkspace and files
		saveWorkspace($workspace);
		
		return "{ \"workspace_id\" : \"".$workspace_id."\" }";
	} else {
		//return proper exception message
	}
}
?>