var app = angular.module('myapp', []);

app.controller('MainCtrl', function($scope, MyService) {
    $scope.mainModel = MyService.myObject;
});

app.controller('OtherCtrl', function($scope, MyService) {
    
});

app.factory('MyService', function() {
  return {
    myObject: {
      name: 'test'
    }
  };
});

app.directive('xyz', function() {	
	return {
		restrict : 'E',
		scope : true,
        templateUrl:'_xyz.html'
	};
});
