appModule.service('TimeLoopService', function(BroadcastService, Constants) {
	this.autoSyncWorkspaceTimer = undefined;
	this.startAutoSyncWorkspaceWithServer = function() {
		this.autoSyncWorkspaceTimer = setInterval(function() {
			BroadcastService.broadcast(Constants.auto_save_operation);
		}, 3000);
	};
	
	this.stopAutoSyncWorkspaceWithServer = function() {
		window.clearInterval(this.autoSyncWorkspaceTimer);
	};
});
