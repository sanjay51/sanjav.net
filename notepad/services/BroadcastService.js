appModule.service('BroadcastService', function($rootScope) {
	this.broadcast = function (message) {
		console.log ("broadcast: " + message);
		$rootScope.$broadcast(message);
	};
});