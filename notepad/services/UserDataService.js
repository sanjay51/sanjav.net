appModule.service('UserDataService', function($rootScope, DBService) {
	this.UserData = {};
	this.UserData.files = [{
		"file_name" : "Untitled 1",
		"data" : ""
	}];
	this.UserData.email = undefined;
	this.UserData.workspace_id = undefined;
	this.UserData.isDataInSyncWithServer = false;

	this.reset = function() {
		this.UserData.files = [{
			"file_name" : "Untitled 1",
			"data" : ""
		}];
		this.UserData.email = undefined;
		this.UserData.workspace_id = undefined;
		this.UserData.isDataInSyncWithServer = false;
	};

	this.getUserEmail = function() {
		return this.UserData.email;
	};

	this.getworkspace_id = function() {
		return this.UserData.workspace_id;
	};

	this.hasWorkspaceId = function() {
		return this.UserData.workspace_id != undefined;
	};

	this.setWorkspaceId = function(id) {
		this.UserData.workspace_id = id;
	};

	this.setUserEmailId = function(email) {
		this.UserData.email = email;
	};

	this.setFiles = function(file_array) {
		this.UserData.files = file_array;
	};

	this.setIsInSyncWithServer = function(isInSync) {
		this.UserData.isDataInSyncWithServer = isInSync;
	};

	this.isInSyncWithServer = function() {
		return this.UserData.isDataInSyncWithServer;
	};

	this.getFiles = function() {
		return this.UserData.files;
	};

	this.getWorkspaceData = function() {
		return this.UserData;
	};

	this.saveWorkspace = function() {
		DBService.saveWorkspace(this.getWorkspaceData());
	};
});
