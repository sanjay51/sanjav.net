appModule.service('Constants', function($rootScope) {
	this.undo_operation = "UNDO_OPERATION";
	this.redo_operation = "REDO_OPERATION";
	this.show_local_history = "SHOW_LOCAL_HISTORY";
	this.cut_operation = "CUT_OPERATION";
	this.open_operation = "OPEN_OPERATION";
	this.rename_file_operation = "RENAME_FILE_OPERATION";
	this.search_in_file_operation = "SEARCH_IN_FILE_OPERATION";
	
	this.save_operation = "SAVE_OPERATION";
	this.save_success = "SAVE_SUCCESS";
	this.unsaved_changes = "UNSAVED_CHANGES";
	this.auto_save_operation = "AUTO_SAVE_OPERATION";
	this.new_workspace_operation = "NEW_WORKSPACE_OPERATION";
	this.open_workspace_operation = "OPEN_WORKSPACE_OPERATION";
	this.open_workspace_operation_2 = "OPEN_WORKSPACE_OPERATION_2";
	this.reset_operation = "RESET_OPERATION";
});