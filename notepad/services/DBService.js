appModule.service('DBService', function($http, BroadcastService, Constants) {
	
	this.loadWorkspace = function(id) {
		return $http.get('db/loadWorkspace.php?id=' + id);
	};
	
	this.saveWorkspace = function(UserData) {
		$http.post('db/saveWorkspace.php', UserData)
		.success(function(response) {
			if (response.workspace_id != undefined) {
			    UserData.workspace_id = response.workspace_id;
			}
			
			BroadcastService.broadcast(Constants.save_success);
		});
	};
	
	this.ok = function() {
		alert('DBService OK');
	};
});
