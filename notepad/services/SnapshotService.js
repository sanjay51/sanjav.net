appModule.service('SnapshotService', function() {
	this.snapshots = {};

	this.addSnapshot = function(tag, file, data) {
		if (this.snapshots[tag] == undefined) {
			this.snapshots[tag] = {};
		}

		if (this.snapshots[tag][file] == undefined) {
			this.snapshots[tag][file] = {};
		}

		if (this.snapshots[tag][file]["revisions"] == undefined) {
			this.snapshots[tag][file]["revisions"] = {};
		}
		
		newRevision = this.snapshots[tag][file].curr_rev_index;
		
		//Update revision index
		if (newRevision == undefined) {
			newRevision = 0;
		} else {
			newRevision = newRevision + 1;
		}
		
		this.snapshots[tag][file].curr_rev_index = newRevision;
		
		//log to console for debugging
		console.log("tag:" + tag + " file:" + file + " newRevision:" + newRevision);
		
		//Store revision data
		revStr = this.getRevisionString(newRevision);
		this.snapshots[tag][file]["revisions"][revStr] = data;
	};

	this.getLastSnapshot = function(tag, file) {
		nextRevision = this.snapshots[tag][file].curr_rev_index;
		if (nextRevision > 0) {
			nextRevision = nextRevision - 1;
			this.snapshots[tag][file].curr_rev_index = nextRevision;
		}
		
		revStr = this.getRevisionString(nextRevision);
		console.log("tag:" + tag + " file:" + file + " revision:" + revStr);
		
		return this.snapshots[tag][file]["revisions"][revStr];
	};

	this.getNextSnapshot = function(tag, file) {
		nextRevision = this.snapshots[tag][file].curr_rev_index + 1;
		revStr = this.getRevisionString(nextRevision);
		
		data = this.snapshots[tag][file]["revisions"][revStr];
		if (data == undefined) {
			nextRevision = nextRevision - 1;
		    revStr = this.getRevisionString(nextRevision);
			data = this.snapshots[tag][file]["revisions"][revStr];
		} else {
		    this.snapshots[tag][file].curr_rev_index = nextRevision;
		}
		
		console.log("tag:" + tag + " file:" + file + " revision:" + revStr);
		
		return data;
	};
	
	this.getAllRevisionsForFile = function(tag, file) {
		return this.snapshots[tag][file];
	};

	this.getAllSnapshots = function() {
		return this.snapshots;
	};

	this.showOff = function() {
		alert(JSON.stringify(this.snapshots));
	};
	
	this.getRevisionString = function(key) {
		return ('00000' + key).substr(-5);
	};
});

//Sample data:
//snapshots = {
//    "tag1" : {                    //tag 1
//        "1" : {                   //file 1
//	          "curr_rev_index" : 2
//            "revisions" : {
//                "1" : "xyz abc",      //revision 1 : data
//                "2" : "xyz abcd"      //revision 2 : data
//             }
//        }
//    }
//];
