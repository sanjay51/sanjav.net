padModule.controller('PadController', function($scope, DBService, UserDataService, Constants, SnapshotService, TimeLoopService, BroadcastService) {
	$scope.filecount = 1;
	$scope.activetab = 0;

	//application state
	$scope.state = {};
	$scope.state.email = "dd";
	$scope.state.files = [];

	//temporary variables
	$scope.revisionKeys = undefined;
	$scope.revisions = {};
	$scope.revisionData = "";

	$scope.temp = {};
	$scope.isSaving = true;

	//open file
	$scope.filename = "";
	$scope.filetext = "";

	$scope.addFileToWorkspace = function(filename, data) {
		$scope.filecount++;
		
		if (filename == undefined) {
			filename = "Untitled " + $scope.filecount;
		}

		if (data == undefined) {
			data = "Some text here.. " + $scope.filecount;
		}

		var newfile = {
			"file_name" : filename,
			"data" : data
		};

		$scope.state.files.push(newfile);
		$scope.activetab = $scope.filecount - 1;
	};

	$scope.loadWorkspaceByID = function(id) {
		DBService.loadWorkspace(id).success(function(response) {
			if (response == "") {
				return;
				//no workspace found with given id
			}

			//TODO: handle warning message from server

			$scope.filecount = response.files.length;
			$scope.activetab = 0;
			$scope.state.files = response.files;

			//save local revision for all files
			angular.forEach($scope.state.files, function(file, index) {
				$scope.saveLocalFileRevision(index);
			});

			//update UserDataService
			UserDataService.setFiles($scope.state.files);

			$scope.currentFileData = $scope.state.files[$scope.activetab]["data"];
		});
	};

	$scope.state_count = 0;
	$scope.stateChanged = function() {
		$scope.state_count += 1;
		if ($scope.state_count >= 2) {
			$scope.saveLocalFileRevision($scope.activetab);
			$scope.state_count = 0;
		}

		BroadcastService.broadcast(Constants.unsaved_changes);
	};

	$scope.tabChanged = function(index) {
		$scope.activetab = index;
	};
	
	$scope.getCurrentFileData = function() {
		return $scope.state.files[$scope.activetab]["data"];
	};
	
	$scope.setCurrentFileData = function(data) {
		$scope.state.files[$scope.activetab]["data"] = data;
	};

	//Utility
	$scope.ok = function(message) {
		alert('ok: ' + message);
	};

	//Open local file related
	$scope.loadLocalFile = function() {
		$scope.dialog.isOpenFileDialogVisible = false;
		var reader = new FileReader();
		var localfile = $scope.getLocalFile();

		reader.onload = function() {
			var contents = reader.result;

			filename = contents;
			filetext = contents;
			$scope.addFileToWorkspace(localfile.name, contents);
			$scope.$digest();
		};
		reader.readAsText(localfile);
	};
	$scope.setLocalFile = function(element) {
		$scope.temp.local_file_element = element;
	};
	$scope.getLocalFile = function() {
		return $scope.temp.local_file_element.files[0];
	};
	
	$scope.$on(Constants.reset_operation, function() {
		$scope.reset();
	});

	$scope.reset = function() {
		$scope.filecount = 1;
		$scope.activetab = 0;

		//application state
		$scope.state = {};
		$scope.state.email = "dd";
		$scope.state.files = [];

		//dialog box visibility
		$scope.dialog = {};
		$scope.dialog.isSaveDialogVisible = false;
		$scope.dialog.isOpenFileDialogVisible = false;
		$scope.dialog.isLocalHistoryDialogVisible = false;
		$scope.dialog.isConfirmDialogVisible = false;
		$scope.dialog.isSuccessAlertVisible = false;
		$scope.dialog.isOpenWorkspaceDialogVisible = false;
		$scope.dialog.isRenameFileDialogVisible = false;

		//temporary variables
		$scope.revisionKeys = undefined;
		$scope.revisions = {};
		$scope.revisionData = "";

		$scope.temp = {};
		$scope.isSaving = true;

		//open file
		$scope.filename = "";
		$scope.filetext = "";
		
		UserDataService.reset();
	};

	//Scope Initialization
	$scope.init = function() {
		$scope.reset();
		
		wsid = $scope.workspaceId;
		if (wsid != undefined && wsid != 0) {
			$scope.loadWorkspaceByID(wsid);
			UserDataService.setWorkspaceId(wsid);
		}
		
		$scope.state = UserDataService.UserData;

		TimeLoopService.startAutoSyncWorkspaceWithServer();
	};

	$scope.init();
});

padModule.directive('padBox', function() {
	return {
		restrict : 'E',
		scope : {
			workspaceId : '='
		},
		controller : "PadController",
		templateUrl : 'pad/_pad.html'
	};
});

padModule.filter('reverse', function() {
	return function(items) {
		var result = {};
		angular.forEach(items, function(value, key) {
			result[key] = value;
		});
		return result;
	};
});
