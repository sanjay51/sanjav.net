padModule.controller('OpenFileDialogController', function($scope, Constants) {
	
	$scope.$on(Constants.open_operation, function() {
		$scope.dialog.isOpenFileDialogVisible = true;
	});
});

padModule.directive('openFileDialog', function() {
	return {
		restrict : 'E',
		scope : {
			isVisible : '='
		},
		scope : false,
		templateUrl : 'dialog/_openfile.html',
		controller : 'OpenFileDialogController'
	};
});
