padModule.controller('OpenWorkspaceDialogController', function($scope, Constants, BroadcastService) {
	
	$scope.$on(Constants.open_workspace_operation, function() {
		$scope.dialog.isOpenWorkspaceDialogVisible = true;
	});
	
	$scope.openWorkspace = function() {
		$scope.dialog.isOpenWorkspaceDialogVisible = false;
		BroadcastService.broadcast(Constants.open_workspace_operation_2);
	};
});

padModule.directive('openWorkspaceDialog', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'dialog/_openworkspace.html',
		controller : 'OpenWorkspaceDialogController'
	};
});
