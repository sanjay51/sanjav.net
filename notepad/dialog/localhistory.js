padModule.controller('LocalHistoryController', function($scope, SnapshotService, Constants) {
	$scope.saveLocalFileRevision = function(fileindex) {
		SnapshotService.addSnapshot(Constants.undo_operation, fileindex, $scope.state.files[fileindex]["data"]);
	};

	$scope.hideLocalHistory = function() {
		$scope.dialog.isLocalHistoryDialogVisible = false;
	};

	$scope.selectRevision = function(key) {
		$scope.revisionData = $scope.revisions[key];
	};

	$scope.selectLocalRevision = function() {
		$scope.state.files[$scope.activetab]["data"] = $scope.revisionData;
		$scope.saveLocalFileRevision($scope.activetab);
		$scope.hideLocalHistory();
	};

	$scope.$on(Constants.undo_operation, function() {
		data = SnapshotService.getLastSnapshot(Constants.undo_operation, $scope.activetab);
		if (data != undefined) {
			$scope.state.files[$scope.activetab]["data"] = data;
		}
	});

	$scope.$on(Constants.redo_operation, function() {
		data = SnapshotService.getNextSnapshot(Constants.undo_operation, $scope.activetab);
		if (data != undefined) {
			$scope.state.files[$scope.activetab]["data"] = data;
		}
	});

	$scope.$on(Constants.show_local_history, function() {
		revisionObj = SnapshotService.getAllRevisionsForFile(Constants.undo_operation, $scope.activetab);
		$scope.revisions = revisionObj["revisions"];
		$scope.revisionKeys = Object.keys($scope.revisions).reverse();
		$scope.revisionData = $scope.revisions["1"];
		$scope.dialog.isLocalHistoryDialogVisible = true;
	});
});

padModule.directive('localHistoryDialog', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'dialog/_localhistory.html',
		controller : 'LocalHistoryController'
	};
});
