padModule.controller('RenameFileDialogController', function($scope, Constants, BroadcastService) {
	$scope.renameFileDialogData = {};

	$scope.$on(Constants.rename_file_operation, function() {
		$scope.dialog.isRenameFileDialogVisible = true;
		$scope.renameFileDialogData.currentfilename = $scope.getCurrentFileName();
	});

	$scope.renameFile = function() {
		$scope.setCurrentFileName($scope.renameFileDialogData.currentfilename);
		$scope.dialog.isRenameFileDialogVisible = false;
	};
	
	$scope.getCurrentFileName = function() {
		return $scope.state.files[$scope.activetab]["file_name"];
	};
	
	$scope.setCurrentFileName = function(name) {
		$scope.state.files[$scope.activetab]["file_name"] = name;
	};
});

padModule.directive('renameFileDialog', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'dialog/_renamefile.html',
		controller : 'RenameFileDialogController'
	};
});
