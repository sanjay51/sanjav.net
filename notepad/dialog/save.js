padModule.controller('SaveDialogController', function($scope, Constants, UserDataService) {
	$scope.saveDialogData = {};
	
	$scope.saveWorkspace = function() {
		UserDataService.setUserEmailId($scope.saveDialogData.email);
		UserDataService.saveWorkspace();
	};

	$scope.$on(Constants.save_success, function() {
		$scope.dialog.isSuccessAlertVisible = true;
		$scope.dialog.isSaveDialogVisible = false;
	});

	$scope.$on(Constants.save_operation, function() {
		if (UserDataService.hasWorkspaceId()) {
			UserDataService.saveWorkspace();
		} else {
			$scope.dialog.isSaveDialogVisible = true;
		}
	});

	$scope.$on(Constants.auto_save_operation, function() {
		if (UserDataService.hasWorkspaceId() && (! UserDataService.isInSyncWithServer())) {
			UserDataService.saveWorkspace();
			UserDataService.setIsInSyncWithServer(true);
			$scope.$apply();
		}
	});

	$scope.$on(Constants.unsaved_changes, function() {
		UserDataService.setIsInSyncWithServer(false);
	});
});

padModule.directive('saveDialogBox', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'dialog/_save.html',
		controller : "SaveDialogController"
	};
});
