padModule.controller('ConfirmDialogController', function($scope, Constants, UserDataService, BroadcastService) {
	$scope.confirmDialog = {};
	$scope.confirmDialog.operation = "";

	$scope.confirmDialog.hideConfirmDialog = function() {
		$scope.dialog.isConfirmDialogVisible = false;
	};

	$scope.confirmDialog.createNewWorkspace = function() {
		window.location.assign("/notepad/");
	};
	
	$scope.confirmDialog.openExistingWorkspace = function(wid) {
		window.location.assign("/notepad/?id=" + wid);
	};
	
	$scope.confirmDialog.executeConfirmDialogOperation = function() {
		if ($scope.confirmDialog.operation == Constants.open_workspace_operation_2) {
			$scope.confirmDialog.openExistingWorkspace($scope.openWorkspace.wid);
		} else if ($scope.confirmDialog.operation == Constants.new_workspace_operation) {
			$scope.confirmDialog.createNewWorkspace();
		}
	};

	$scope.$on(Constants.new_workspace_operation, function() {
		$scope.confirmDialog.operation = Constants.new_workspace_operation;
		
		if (! UserDataService.isInSyncWithServer()) {
			$scope.dialog.isConfirmDialogVisible = true;
		} else {
			$scope.confirmDialog.executeConfirmDialogOperation();
		}
	});

	$scope.$on(Constants.open_workspace_operation_2, function() {
		$scope.confirmDialog.operation = Constants.open_workspace_operation_2;
		
		if (! UserDataService.isInSyncWithServer()) {
			$scope.dialog.isConfirmDialogVisible = true;
		} else {
			$scope.confirmDialog.executeConfirmDialogOperation();
		}
	});
});

padModule.directive('confirmDialogBox', function() {
	return {
		restrict : 'E',
		controller : "ConfirmDialogController",
		templateUrl : 'dialog/_confirm.html'
	};
});
