padModule.directive('dialogBox', function() {
	return {
		restrict : 'E',
		scope : {
			title : '@'
		},
		transclude : true,
		templateUrl : 'dialog/_dialogBox.html'
	};
});
