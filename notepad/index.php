<html>
	<head>
		<title>Online notepad - notepad.sanjav.net</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<!-- Include the Bootstrap library -->
		<link href="libs/bootstrap.min.css" rel="stylesheet" />
		<script src="libs/jquery.min.js"></script>
		<script src="libs/bootstrap.min.js"></script>
		<link href="dialog/style.css" rel="stylesheet" />

		<!-- Include the AngularJS library -->
		<script src="libs/angular.min.js"></script>
		
	</head>
	<body ng-app="notepadApp" ng-init="wid=<?php echo $_GET['id'] == "" ? 0 : $_GET['id']; ?>">
		<div class="container" ng-controller="notepadAppController" align="center" style="height:100%; overflow-y: scroll;">
			<header-box></header-box>
			<br/>
			<pad-box workspace-id="wid"></pad-box>
		</div>

		<!-- Modules -->
		<script src="app.js"></script>

		<!-- Controller -->
		<script src="notepadAppController.js"></script>

		<!-- Components -->
		<script src="header/header.js"></script>
		<script src="pad/pad.js"></script>
		<script src="dialog/confirm.js"></script>
		<script src="dialog/localhistory.js"></script>
		<script src="dialog/openfile.js"></script>
		<script src="dialog/save.js"></script>
		<script src="dialog/openworkspace.js"></script>
		<script src="dialog/dialogBox.js"></script>
		<script src="dialog/renamefile.js"></script>
		<script src="panel/searchpanel.js"></script>

		<!-- Services -->
		<script src="services/UserDataService.js"></script>
		<script src="services/DBService.js"></script>
		<script src="services/BroadcastService.js"></script>
		<script src="services/SnapshotService.js"></script>
		<script src="services/Constants.js"></script>
		<script src="services/TimeLoopService.js"></script>
	</body>
</html>